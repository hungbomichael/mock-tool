
## About the Chronologue API
<br>

The Chronologue API is open-source interface that provides data about astronomical events. You can easily query astronomical data stored in the API and visualize events on the Chronologue website. 

The main purpose of the Chronologue API is to help scientists and organizations advance humanity's knowledge. To query and visualize an astronomical event, you need an HTTP client, for example, a web browser. 

The Chronologue API follows a REST architecture. You can access the API's resources via HTTP requests and responses are given in JSON format. 
 

<br>

## Main features
<br>

The Chronologue telescope records astronomical events of the past and the future. It stores the events in a [data.json] file (https://github.com/thegooddocsproject/chronologue/blob/main/data/data.json) as event objects. You can use the Chronologue API to query exisiting astronomical events from the `data.json file`. 

If you want to retrieve data for an event that does not yet exist, you have to make a request to record a new recording. 

> Note: The Chronologue telescope predicts future events, but the accuracy of these predictions is not guaranteed. 

Read and follow the steps described in the "Request for a new recording" document. Fill out and submit the provided form and, if your request is approved, you will receive a notification email with access to the Chronologue API. 


<br>


 
![Chronologue API workflow](https://drive.google.com/file/d/12_D--AJbQu7b8yJQFHbnTMpBIo_mZ9G5/view?usp=sharing)
A high-level diagram of the Chronologue API's workflow. 



<br>
<br>

## Use cases
<br>

- **Academic research**

Gather information with the Chronologue API to further humanity's understanding. You can visualize past events that can help you explain what circumstances caused a another phenomenon of nature. 
 
Use the Chronologue API to test your hypothesis. The Chronologue API can make predictions about future astronomical events. You can compare your predictions to those prouced by the API as a way to validate your hypothesis. 

<br>

- **Educational purposes**

Demonstrate to your audience the causal relationship between two natural events. For example, you can show your students the asteroid's impact on Earth during the Cretaceous Period that caused the mass extinction of three-quarters of plant and animal species. 

----------------------------------------------------------------------------------------------

## How to contribute 

Developers can contribute to the software by joining the Chronologue Project, which is part of The Good Docs project. To contribute to program code, you should be familiar with the following tools:

- Next.js
- Netlify hosting infrastructure

To learn more, join our [Slack community](https://thegooddocs.slack.com/) or attend a [Chronologue meeting](https://thegooddocsproject.dev/community/).
