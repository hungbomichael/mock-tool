# Chronologue App plan

This document lays down some fundamentals regarding the Chronologue app.
The API data is stored in this repository and deployed with Netlify. 

## Who will find Chronologue useful 
- Casual readers
- Developers
- Technical writers

## App plan: About The Chronologue API

## Licence 
The API will use Zero clause BSD.

## Types of Documentation needed

> Note: The documents mentioned below are part of the documentation plan. We list the documents here as well for a better overview. 

The API needs the following documentation: 
- API reference: Endpoints and routes
- Concept explanation guide
- Quick start guide
- Bug reporting guide

## Future features
**Note**: Since the app is hosted on Netlify, all future feature implementations must be compatible with Netlify. 

- User authentication using OAuth 2.0 
    - Current plan is to use NextAuth.js or other Netlify-supported plugin to enable authentication.
- ~~API cache will be handled by Next.js and Netlify~~ 

# Contributing
You can start contributing to the following parts of the repository: 
- API data, located in: `/data/fragments`
- Next.js web view and app logics. You can help make The Chronologue look more polished and functional by contributing to program code. You should be familiar to the following concepts:
    - React library
    - Next.js framework
    - Netlify hosting infrastructure
